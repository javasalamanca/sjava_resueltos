import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import axios from 'axios';

import Button from '@material-ui/core/Button';

var alumnos = [];

axios.get('http://localhost:8080/alumno/id', {
  params: {
    id: 1
  }
})
.then(function (response) {
  alumnos = response;
  console.log(response);
})
.catch(function (error) {
  console.log(error);
});


const TableRow = ({row}) => (
  <tr>
    <td key={row.nombre}>{row.nombre}</td>
    <td key={row.email}>{row.email}</td>
    <td key={row.ciudad}>{row.ciudad}</td>
  </tr>
)

const Table = ({data}) = (
  <table>
    {data.map(row => {
      <TableRow row={row} />
    })}
  </table>
)



class App extends Component {


  render() {
    
    return (
     
      <div className="App">
      <Table data={alumnos} />
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <Button variant="contained" color="primary">
          Hello World
        </Button>
      </div>
    );
  }
}

export default App;
