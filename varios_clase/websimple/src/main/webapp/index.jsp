<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sjava.web.*" %>


<%
    String titulo = "Mi primer JSP";
    String[] lista = new String[] {"primero", "segundo", "tercero"};


%>
<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta charset="utf-8">
    <title>Ejercicios HMTL/CSS</title>
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
</head>
<body>
<h1><% out.print(titulo); %></h1>
<h1><%= titulo %></h1>
<br>
<ul>
    <% for (String s : lista) { %>
        <li><%= s %></li>
    <% } %>
</ul>

<table border="1" width="50%">
<tr><th width="60%">Nombre</th><th width="30%">Edad</th><th width="30%"></th></tr>

<% for(Persona p : PersonaController.getContactos() ) { %>
<tr>
    <td><%= p.getNombre() %></td>
    <td><%= p.getAnyo() %></td>
    <td><a href="/websimple/detalle.jsp?codigo=<%= p.getId() %>&nombre=<%= p.getNombre() %>">Detalle</a></td>
</tr>
<% } %>
</table>

</body>
</html>