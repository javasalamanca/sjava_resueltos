
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import java.util.Map.Entry;

class MapaPersona {


    public static void main(String[] args) {
        
        Map<Integer,Persona> contactos = new TreeMap<Integer, Persona>();

        Persona p1 = new Persona(92000, "Manuel", "Béjar");
        Persona p2 = new Persona(92012, "Ana", "Santa Marta");
        Persona p3 = new Persona(92022, "Marta", "Santa Ana");

        contactos.put(p1.telefono, p1);
        contactos.put(p2.telefono, p2);
        contactos.put(p3.telefono, p3);

        for (Entry contacto : contactos.entrySet()){
            Persona p = (Persona) contacto.getValue();
            System.out.printf(
                "(%d) %s - %s\n", contacto.getKey(), p.nombre, p.ciudad
            );
        }

        Persona personaX = contactos.get(92012);
        System.out.println("La persona 92012 es: "+personaX.nombre);
        
        Properties sysProp = System.getProperties();
        
        for (Entry prop : sysProp.entrySet()){
           
            System.out.printf(
                "%s : %s\n", prop.getKey(), prop.getValue()
            );
        }

    }
}
