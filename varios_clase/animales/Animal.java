

public class Animal {
    public String nombre = "Ninguno";
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public  String tipo() {
        return "animal";
    }
}
