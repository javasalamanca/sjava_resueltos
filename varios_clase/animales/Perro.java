
public class Perro extends Animal {
    public String nombre = "Scooby";

    @Override
    public String getNombre() {

        return nombre;
    }
    
    @Override
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String tipo() {
        return "perro";
    }

}
