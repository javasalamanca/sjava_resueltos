

class Persona {

    private String nombre;
    private int edad;

    public static int max_personas = 100;

    
    public static String define() {
        return "Soy una clase Persona";
    }


    public Persona(String nombre) {
        this.nombre = nombre;
    }

    public void setEdad(int edad){
        if (edad<0 || edad>150){
            edad = 30;
        }
        
        this.edad = edad;
    }

    public int getEdad() {
        return this.edad;
    }

    public String getNombre() {
        return this.nombre;
    }
}