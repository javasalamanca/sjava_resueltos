import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

class DateUtil {

    private DateUtil(){}

    public static Date createDate(int anyo, int mes, int dia) {
        Calendar cal = Calendar.getInstance();
        cal.set(anyo,mes-1,dia);
        return cal.getTime();
    }

    public static Date createDateTime(int anyo, int mes, int dia, int horas, int minutos, int segundos) {
        Calendar cal = Calendar.getInstance();
        cal.set(anyo, mes-1, dia, horas, minutos, segundos);
        return cal.getTime();
    }

    public static String formatDate(Date fecha, String plantilla){
            SimpleDateFormat sdf = new SimpleDateFormat(plantilla);
            return sdf.format(fecha);
    }

    public static int cuentaDomingos(int anyo, int mes){
        int domingos = 0;
        int mesactual = mes - 1;
        Calendar cal = Calendar.getInstance();
        cal.set(anyo, mesactual, 1);
        while (cal.get(Calendar.MONTH)==mesactual) {
            if (cal.get(Calendar.DAY_OF_WEEK)==1) {
                domingos++;
                System.out.println(cal.getTime());
            }
            cal.add(Calendar.DATE, 1);
        }
        return domingos;
    }








    public static String fechaIdioma(Date fecha, String local) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(fecha);

        StringBuilder sb = new StringBuilder();
        Locale idioma = Locale.forLanguageTag(local);

        String mes = cal.getDisplayName(Calendar.MONTH, Calendar.LONG, idioma);
        String diasemana = cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, idioma);
        
        sb.append(diasemana);
        sb.append(" ");
        sb.append(cal.get(Calendar.DAY_OF_MONTH));
        sb.append(" ");
        sb.append(mes);
        sb.append(" de ");
        sb.append(cal.get(Calendar.YEAR));
        return sb.toString();

    }



}