package com.sjava.utilidades;

import java.util.Scanner;

public class Teclado {

    public static Scanner key = new Scanner(System.in);

    public static int pideNumero(String msg, int max, int min){
        int respuesta;
        do {
            System.out.println(msg);
            respuesta = key.nextInt();
            key.nextLine();
        } while( respuesta<min || respuesta>max );
        return respuesta;
    }

    public static String pidePalabra(String msg){
        System.out.println(msg);
        String respuesta = key.nextLine();
        return respuesta;
    }

}
