public class Coche extends Vehiculo {

    public int tipoMotor;
    public int numeroPuertas;
    public int cv;

    public Coche(String marca, int cv){
        super(marca);
        this.cv = cv;
    }
}
