class Test {

    public static void main(String[] args) {
        Persona h = new Hombre("Mariano", 50);
        Persona m = new Mujer("Ana", 50);
        
        // errror, no instanciable
        //Persona p = new Persona("Juan", 23);

        System.out.println(h.presentate());
        System.out.println(m.presentate());
        
    }

}