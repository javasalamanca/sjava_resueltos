

abstract class Persona {

    protected String nombre;
    protected int edad;

    Persona(String nombre, int edad) {
        this.nombre = nombre;
        this.edad = edad;
    }

    public abstract String getNombre();
    public abstract int getEdad();

    public String presentate(){
        return "Hola, soy "+this.getNombre()+" y tengo "+this.getEdad()+" años.";
    }


}