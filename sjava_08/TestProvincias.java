import java.util.TreeSet;
import java.util.Set;

class TestProvincias {

    public static void main(String[] args) {
        
        Datos basedatos = new Datos();
        String[] provincias = basedatos.getProvincias();
        TreeSet<String> ordenadas = new TreeSet<String>();
        
        for (String provincia : provincias) {
            ordenadas.add(provincia);
        }

        for (String provincia : ordenadas) {
            System.out.println(provincia);
        }

    }


}
