package com.sjava.testmoto;

class Moto {
    String marca;
    String modelo;
    int cc;
    int cv;
    int pvp;

    public Moto(String marca, String modelo, int cc, int cv, int pvp) {
        this.marca = marca;
        this.modelo = modelo;
        this.cc = cc;
        this.cv = cv;
        this.pvp = pvp;
    }

    public void comparaCv(Moto otra){
        if (this.cv > otra.cv) {
            System.out.printf(
                "La %s %s es más potente que la %s %s",
                this.marca, this.modelo, otra.marca, otra.modelo);
        } else {
            System.out.printf(
                "La %s %s es más potente que la %s %s",
                otra.marca, otra.modelo, this.marca, this.modelo);   
        }
    }

    public void comparaPrecio(Moto otra){
        int diferencia;
        if (this.precio > otra.precio) {
            diferencia = this.precio - otra.precio;

            
            System.out.printf(
                "La %s es %d más cara que la %s",
                this.marca, diferencia, otra.marca);
        } else {
            diferencia = otra.precio - this.precio;
            System.out.printf(
                "La %s es %d más cara que la %s",
                otra.marca, diferencia, this.marca);  
        }
    }

}
