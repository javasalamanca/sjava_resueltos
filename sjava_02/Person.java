
class Person {

    String nombre;
    int edad;

    Person(String nombre, int edad){
        this.nombre = nombre;
        this.edad = edad;
    }

    void presentacion(){
        System.out.println("Hola me llamo " + this.nombre);
    }


    @Override
    public String toString(){
        return this.nombre + " (" + this.edad +")" ;
    }

}