

import java.util.Random;
import java.util.Scanner;

/**
 * Juego tic-tac-toe
 * 
 * Método principal "play" Curso Java Salamanca Mayo/Junio 2018 Mini-Proyecto
 * 
 * @author XXX
 * @version 1.0
 * 
 */
class Tictactoe {

    // constantes utilizadas en el código
    final String[] SIMBOLOS = new String[] { ".", "X", "O" };
    final int JUGADOR1 = 1;
    final int JUGADOR2 = 2;
    final int GANA1 = 1;
    final int GANA2 = 2;
    final int EMPATE = 0;
    final int SEGUIR = -1;
    final int NOSEPUEDE = 98457364;
    final int FULLMAP = 0;
    final int HUMAN_HUMAN = 1;
    final int HUMAN_ROBOT = 2;
    final int ROBOT_HUMAN = 3;
    final int ROBOT_ROBOT = 4;

    // array 2D (matriz) de posiciones ganadoras
    final int[][] WINNERS = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 }, { 1, 4, 7 }, { 2, 5, 8 }, { 3, 6, 9 }, { 1, 5, 9 },
            { 7, 5, 3 } };

    // mapa de posiciones ocupadas, un array de 9 enteros indicando:
    // (0) posición libre (1) jugador1 (2) jugador2
    int[] map = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };

    // inicializamos keyboard como atributo de clase para poderlo utilizar en varios
    // métodos
    Scanner keyboard = new Scanner(System.in);

    int modo_juego = 0;

    int menu() {
        int modo = 0;
        do {
            try {
                System.out.println("\n\n\n");
                System.out.println("TIPO DE PARTIDA");
                System.out.println("----------------------");
                System.out.println("(1) Jugador vs Jugador");
                System.out.println("(2) Jugador vs Robot");
                System.out.println("(3) Robot vs Jugador");
                System.out.println("(4) Robot vs Robot");
                System.out.println("----------------------");
                System.out.printf("Opción: ");
                modo = keyboard.nextInt();
            } catch (Exception e) {
                // ...
                modo = 0;
            }
        } while (modo < 1 || modo > 4);

        return modo;
    }

    /**
     * Método principal Crea un bucle indefinido en que ejecuta los dos turnos, uno
     * para cada jugador Siempre empieza el jugador 1 En cada turno se llama al
     * método "turno" que pide la jugada y redibuja el mapa
     * 
     */

    public void play() {

        int respuesta;
        // mostramos el menú de opciones
        modo_juego = menu();

        // dibujamos tablero por primera vez
        draw();

        do {
            // invocamos turno con número de jugador y recibimos respuesta
            if (modo_juego == HUMAN_HUMAN || modo_juego == HUMAN_ROBOT) {
                respuesta = turnoJugador(JUGADOR1);
            } else {
                respuesta = turnoRobot(JUGADOR1);
            }

            // si respuesta es distinta de SEGUIR, saltamos el turno 2
            if (respuesta != SEGUIR)
                continue;

            // turno 2, ejecutamos y esperamos respuesta
            if (modo_juego == ROBOT_HUMAN || modo_juego == HUMAN_HUMAN)
                respuesta = turnoJugador(JUGADOR2);
            else
                respuesta = turnoRobot(JUGADOR2);

        } while (respuesta == SEGUIR);

        // hemos salido del bucle, por tanto tenemos o ganador o empate
        // mostramos mensaje adecuado
        switch (respuesta) {
        case GANA1:
            System.out.println("Gana el jugador 1");
            System.out.println("=================");
            break;
        case GANA2:
            System.out.println("Gana el jugador 2");
            System.out.println("=================");
            
            break;
        default:
            System.out.println("Empate, no hay más posiciones");
            System.out.println("-----------------------------");
            
            break;
        }

        // cerramos teclado, aunque no es imprescindible
        keyboard.close();
    }

    /**
     * método que ejecuta la jugada: pide posición al jugador y devuelve resultado
     * A COMPLETAR: debería verificar si la posición está ocupada, y en este caso vo
     *  verla a pedir
     */
    int turnoJugador(int jugador) {
        int resp;
        int posicion = 0;
        do {
            // mostramos pregunta y esperamos número introducido
            System.out.printf("Jugador %d (%s): ", jugador, SIMBOLOS[jugador]);
            try {
                posicion = keyboard.nextInt();
            } catch (Exception e) {
                draw();
                System.out.println("**ERROR**");
                keyboard.next();
                posicion = 0;
                continue;
            }
            if (posicion < 1 || posicion > 9) {
                draw();
                System.out.println("**POSICION FUERA DE RANGO 1..9**");
                posicion = 0;
                continue;

            }
            if (getMap(posicion) > 0) {
                draw();
                System.out.println("**POSICION OCUPADA**");
                continue;
            }
        } while (posicion == 0 || getMap(posicion) > 0);

        // establecemos posición en el mapa para jugador actual
        setMap(posicion, jugador);

        // mostramos mapa actualizado
        draw();

        // calcula respuesta que debe retornar
        if (winner(jugador)) {
            resp = (jugador == JUGADOR1) ? GANA1 : GANA2;
        } else if (numZeros() == 0) {
            resp = EMPATE;
        } else {
            resp = SEGUIR;
        }

        return resp;
    }

    /**
     * mini "IA" turno automático para modalidad un jugador
     */
    int turnoRobot(int jugador) {

        int contrario = (jugador == JUGADOR1) ? JUGADOR2 : JUGADOR1;
        int resp;

        int posicion = posicionGanadora(jugador);
        if (posicion == NOSEPUEDE) {
            posicion = posicionGanadora(contrario);
            if (posicion == NOSEPUEDE) {
                posicion = posicionAleatoria();
            }
        }

        // posicionAleatoria() no debería invocarse nunca si el mapa está lleno, pero...
        if (posicion == FULLMAP)
            return EMPATE;

        // establecemos posición en el mapa para jugador actual
        setMap(posicion, jugador);

        // mostramos mapa actualizado
        draw();

        // calcula respuesta que debe retornar
        if (winner(jugador)) {
            resp = (jugador == JUGADOR1) ? GANA1 : GANA2;
        } else if (numZeros() == 0) {
            resp = EMPATE;
        } else {
            resp = SEGUIR;
        }

        return resp;
    }

    /**
     * método que verifica si en las posiciones actuales del tablero el jugador 
     * recibido es el ganador, y en este caso devolver true, de lo contrario false A
     * COMPLETAR: debe verificar realmente las jugadas! pista: utilizar el array
     * WINNERS... final int[][] WINNERS = { {1,2,3}, {4,5,6}, {7,8,9}, {1,4,7}, map
     * = {1,2,0, 1,1,1, 2,2,0}
     */
    boolean winner(int jugador) {
        for (int[] trio : WINNERS) {
            if (getMap(trio[0]) == jugador && getMap(trio[1]) == jugador && getMap(trio[2]) == jugador) {
                return true;
            }
        }
        return false;
    }

    // devuelve una posición random válida, o FULLMAP si mapa está lleno
    int posicionAleatoria() {
        Random random = new Random();
        int randompos;
        // si mapa lleno no podemo seguir
        if (numZeros() == 0)
            return FULLMAP;
        // buscamos posición libre
        do {
            randompos = random.nextInt(9) + 1;
        } while (getMap(randompos) > 0);

        return randompos;
    }

    // checkea si el jugador esta a una posición de ganar la partida
    // devuelve la posición o NOSEPUEDE 
    int posicionGanadora(int jugador) {

        int oks, zeros, winner_pos;
        winner_pos = NOSEPUEDE;

        for (int[] trio_posiciones : WINNERS) {
            int[] trio_valores = { getMap(trio_posiciones[0]), getMap(trio_posiciones[1]), getMap(trio_posiciones[2]) };
            oks = zeros = 0;
            for (int i = 0; i < trio_valores.length; i++) {
                if (trio_valores[i] == jugador) {
                    oks++;
                } else if (trio_valores[i] == 0) {
                    zeros++;
                    // guardamos la posición donde hemos encontrado un cero
                    winner_pos = trio_posiciones[i]; // IMPORTANTE
                }

            }
            // sólo es apuesta ganadora si tenemos 2 "oks" y 1 "zero"!!!
            if (oks == 2 && zeros == 1) {
                return winner_pos; // devolvemos posición donde jugar para ganar!
            }
        }
        return NOSEPUEDE;
    }

    // devuelve el número de posiciones 0 que hay en el tablero
    // si no queda ninguna, significará que se ha terminado la partida (devuelve 0)
    int numZeros() {
        int zeros = 0;
        for (int i : map) {
            if (i == 0)
                zeros++;
        }
        return zeros;
    }

    // establece la posición del mapa al valor recibido (1/2 segun jugador)
    // IMPORTANTE restamos 1 a la posición puesto que las posiciones van de 1 a 9 y 
    // l array de 0 a 8!
    void setMap(int donde, int valor) {
        this.map[donde - 1] = valor;
    }

    // devuelve el valor que hay en la posición recibida. 
    // restamos también una unidad a la posición!
    int getMap(int posicion) {
        return this.map[posicion - 1];
    }

    // muesta mapa en pantalla, MEJORABLE
    // final String[] SIMBOLOS = new String[] {"-","X","O"};
    public void draw() {
        limpiaConsola();

        String l1 = "  | " + this.SIMBOLOS[getMap(1)] + "  " + this.SIMBOLOS[getMap(2)] + "  " + this.SIMBOLOS[getMap(3)] + " |";
        String l2 = "  | " + this.SIMBOLOS[getMap(4)] + "  " + this.SIMBOLOS[getMap(5)] + "  " + this.SIMBOLOS[getMap(6)] + " |";
        String l3 = "  | " + this.SIMBOLOS[getMap(7)] + "  " + this.SIMBOLOS[getMap(8)] + "  " + this.SIMBOLOS[getMap(9)] + " |";
        String player1 = (modo_juego==HUMAN_HUMAN || modo_juego==HUMAN_ROBOT) ? "HUMAN" : "ROBOT";
        String player2 = (modo_juego==HUMAN_HUMAN || modo_juego==ROBOT_HUMAN) ? "HUMAN" : "ROBOT";
        
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println("Tic-Tac-Toe");
        System.out.println("-----------");
        System.out.println("Jugador 1: " + player1);
        System.out.println("Jugador 2: " + player2);
        System.out.println();
        System.out.println("  -----------");
        System.out.println(l1);
        System.out.println(l2);
        System.out.println(l3);
        System.out.println("  -----------");

        System.out.println();

    }


    public void limpiaConsola() {
		System.out.print("\033[H\033[2J");  
	    	System.out.flush(); 
    }
    
}
