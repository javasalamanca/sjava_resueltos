
import java.util.Scanner;


class PideNums {

        
    public static void main(String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        int total = 0;
        int num=999;
        do {
            System.out.printf("Entra un num: ");
            try {  
                    num = keyboard.nextInt(); 
                    total += num; 
                } catch (Exception e) {  
                    System.out.println("***Dato incorrecto - 0 para salir***");
                    keyboard.next(); //IMPORTANTE! elimina valor incorrecto del buffer del teclado
                    
                }  
        } while (num>0);

        System.out.println("La suma es "+ total);
        keyboard.close();
    }

}